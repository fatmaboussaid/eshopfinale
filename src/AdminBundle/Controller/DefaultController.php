<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $countBoutiques=$em->getRepository('WebBundle:Utilisateurs')->findBy(array());
        //$countBoutiques=count($countBoutiques);
        //dump($countBoutiques); die();
        return $this->render('AdminBundle:Default:index.html.twig',  array(
            //"countBoutiques"=>count($countBoutiques),
            //"listBoutiques"=>$countBoutiques
        ));
    }
}
