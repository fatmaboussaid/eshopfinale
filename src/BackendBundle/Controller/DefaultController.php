<?php

namespace BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/filtrer_grossiste", name="filtrer_grossiste")
     */
    public function FiltrerGrossisteAction(Request $request) {

        $start = $request->get('startDate');
        $endd = $request->get('endDate');
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $commandeValidee = $em->createQuery('SELECT COUNT(c) FROM WebBundle:Commandes c WHERE c.date_validation BETWEEN :start AND :endd
                      AND c.grossiste_id = :userr 
                      AND c.status = 2')
            ->setParameter('start', $start)
            ->setParameter('endd',$endd)
            ->setParameter('userr',$user)
            ->getResult();

        $commandeEnValidee = $em->createQuery('SELECT COUNT(c) FROM WebBundle:Commandes c WHERE c.date_validation BETWEEN :start AND :endd
                      AND c.grossiste_id = :userr 
                      AND c.status = 0')
            ->setParameter('start', $start)
            ->setParameter('endd',$endd)
            ->setParameter('userr',$user)
            ->getResult();

        return $this->render('BackendBundle:grossiste:dashboard_content.html.twig',
            array(
                "cv"=>$commandeValidee,
                "cev"=>$commandeEnValidee));



    }
    /**
     * @Route("/dashboard", name= "admin_dashboard")
     */
    public function indexAction()
    {
        $authChecker = $this->container->get('security.authorization_checker');
        if ($authChecker->isGranted('ROLE_GESTIONNAIRE')) {

            $em = $this->getDoctrine()->getManager();
            $nbProduit = $em->getRepository('WebBundle:Produits');
            $nbCommande = $em->getRepository('WebBundle:Commandes');

            $nbuser = $em->getRepository('WebBundle:Utilisateurs')->findAll();
            $qb = $em->createQueryBuilder()
                ->select('count(delegation.id)')
                ->from('WebBundle:Delegation','delegation')
            ->Where('delegation.grossiste_id IS NULL');

            $delegation = $qb->getQuery()->getSingleScalarResult();


            return $this->render('BackendBundle:gestionnaire:gestionnaire.html.twig',
                array(
                    'nbprod' => $nbProduit->nombreProduit(),
                    'nbcmd' => $nbCommande->nombreCommande(),
                    'nbuser' => $nbuser,
                    'delegation' => $delegation
                )
            );
        } else if($authChecker->isGranted('ROLE_GROSSISTE')){
            $em = $this->getDoctrine()->getManager();
            $commandeValidee = $em->getRepository('WebBundle:Commandes')->findBy(array("status"=>2,"grossiste_id"=>$this->getUser()));
            $commandeEnValidee = $em->getRepository('WebBundle:Commandes')->findBy(array("status"=>0,"grossiste_id"=>$this->getUser()));
            $nbProduit = $em->getRepository('WebBundle:Commandes');
            return $this->render('BackendBundle:grossiste:grossiste.html.twig',
                array(
                    "cv"=>count($commandeValidee),
                    "cev"=>count($commandeEnValidee))
                );
        }
        elseif ($authChecker->isGranted('ROLE_SUPER_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $countBoutiques=$em->getRepository('WebBundle:Utilisateurs')->findBy(array());
            $listBoutiques=$em->getRepository('WebBundle:Boutique')->findBy(array());
            //dump($countBoutiques); die();
            return $this->render('AdminBundle:Default:index.html.twig'/* ,  array(
                "countBoutiques"=>count($countBoutiques),
                "listBoutiques"=>$listBoutiques
            ) */);
        }
        else {
           // $em = $this->getDoctrine()->getManager();
            //$countBoutiques=$em->getRepository('WebBundle:Utilisateurs')->findBy(array());
            //$countBoutiques=count($countBoutiques);
            //dump($countBoutiques); die();
            return $this->render('BackendBundle:Default:index.html.twig',  array(
                
               
            ));
       
        }
    }

}
