<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commandes
 *
 * @ORM\Table("commandes")
 * @ORM\Entity(repositoryClass="WebBundle\Repository\CommandesRepository")
 */
class Commandes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\Utilisateurs", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=false)
     */

    private $utilisateur;
    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\UtilisateursAdresses", inversedBy="commandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adresse;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reference", type="string", unique=true)
     */
    private $reference;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_valid_client", type="datetime")
     */
    private $datevalidclient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_valid_gest", type="datetime",nullable=true)
     */
    private $datevalidgest;

    /**
     * @var string
     *
     * @ORM\Column(name="status_client", type="integer")
     */
    private $status_client = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="status_gest", type="integer",nullable=true)
     */
    private $statusgest = 0;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $delivery_date;


    public  function getStatusClientToString()
    {
        return $this->statusclient_to_string[$this->status_client];
    }
    public  function getStatusGestToString()
    {
        return $this->statusgest_to_string[$this->status_gest];
    }

    public  function getStatusClientCssClass()
    {
        return $this->statusclient_css_class[$this->status_client];
    }
    public  function getStatusGestCssClass()
    {
        return $this->getStatusGestCssClass()[$this->status_gest];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * @param mixed $utilisateur
     */
    public function setUtilisateur(Utilisateurs $utilisateur)
    {
        $this->utilisateur = $utilisateur;
    }

    /**
     * @return \DateTime
     */
    public function getDateValidClient()
    {
        return $this->datevalidclient;
    }

    /**
     * @param \DateTime $date_valid_client
     */
    public function setDateValidClient($datevalidclient)
    {
        $this->datevalidclient = $datevalidclient;
    }

    /**
     * @return \DateTime
     */
    public function getDateValidGest()
    {
        return $this->datevalidgest;
    }

    /**
     * @param \DateTime $date_valid_gest
     */
    public function setDateValidGest($datevalidgest)
    {
        $this->datevalidgest = $datevalidgest;
    }

    /**
     * @return string
     */
    public function getStatusClient()
    {
        return $this->status_client;
    }

    /**
     * @param string $status_client
     */
    public function setStatusClient($status_client)
    {
        $this->status_client = $status_client;
    }

    /**
     * @return string
     */
    public function getStatusGest()
    {
        return $this->statusgest;
    }

    /**
     * @param string $status_gest
     */
    public function setStatusGest($statusgest)
    {
        $this->statusgest = $statusgest;
    }

    /**
     * @return mixed
     */
    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }

    /**
     * @param mixed $delivery_date
     */
    public function setDeliveryDate($delivery_date)
    {
        $this->delivery_date = $delivery_date;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return \DateTime
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param \DateTime $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }



























}
