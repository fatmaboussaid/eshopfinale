<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Produits
 *
 * @ORM\Table("produits")
 * @ORM\Entity(repositoryClass="WebBundle\Repository\ProduitsRepository")
 * @Vich\Uploadable
 */
class Produits
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=125)
     */
    private $nom;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    /**   
     *
     * 
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image = null;
    
    /**
     * @Vich\UploadableField(mapping="produits_images", fileNameProperty="image")
     * @Assert\Expression("this.getImageFile() or this.getImage()", message="admin.validation.image.upload")
     * @Assert\File(maxSize="2000000")
     * @var File
     */
    private $imageFile;
    /**
     * @var string
     *
     * @ORM\Column(name="image1", type="string", length=255, nullable=true)
     */
    private $image1 = null;

    /**
     * @Vich\UploadableField(mapping="produits_images", fileNameProperty="image1")
     * @Assert\Expression("this.getImageFile1() or this.getImage1()", message="admin.validation.image.upload")
     * @Assert\File(maxSize="2000000")
     * @var File
     */
    private $imageFile1;
    /**
     *
     *
     * @var string
     *
     * @ORM\Column(name="image2", type="string", length=255, nullable=true)
     */
    private $image2 = null;

    /**
     * @Vich\UploadableField(mapping="produits_images", fileNameProperty="image2")
     * @Assert\Expression("this.getImageFile2() or this.getImage2()", message="invalid image")
     * @Assert\File(maxSize="2000000")
     * @var File
     */
    private $imageFile2;
    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;
    /**
     * @var float
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nb_achat;
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $is_active = true;

    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\Categories", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $categorie;
    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\Boutique", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $boutique;

    /**
     *
     *
     * @var string
     *
     * @ORM\Column(name="fichtech", type="string", length=255, nullable=true)
     */
    private $fichtech;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="produits_lib", fileNameProperty="fichtech")
     *
     * @var File
     */
    private $file;


    
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Produits
     */
    public function setNom($nom = Null)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Produits
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Produits
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }



    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Produits
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set nbAchat
     *
     * @param integer $nbAchat
     *
     * @return Produits
     */
    public function setNbAchat($nbAchat)
    {
        $this->nb_achat = $nbAchat;

        return $this;
    }

    /**
     * Get nbAchat
     *
     * @return integer
     */
    public function getNbAchat()
    {
        return $this->nb_achat;
    }

    /**
     * Set categorie
     *
     * @param \WebBundle\Entity\Categories $categorie
     *
     * @return Produits
     */
    public function setCategorie(\WebBundle\Entity\Categories $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \WebBundle\Entity\Categories
     */
    public function getCategorie()
    {
        return $this->categorie;
    }


    public function getPathImage()
    {
        return "/uploads/produits/" . $this->getImage();
    }
    /**
     * Set categorieId
     *
     * @param integer $categorieId
     *
     * @return Produits
     */


    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }


    
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
    }


    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @return string
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage1($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return File
     */
    public function getImageFile1()
    {
        return $this->imageFile1;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image1
     *
     * @return Product
     */
    public function setImageFile1($imageFile1 )
    {
        $this->imageFile1 = $imageFile1;
    }

    /**
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param string $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }

    /**
     * @return File
     */
    public function getImageFile2()
    {
        return $this->imageFile2;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image2
     *
     * @return Product
     */
    public function setImageFile2( $imageFile2)
    {
        $this->imageFile2 = $imageFile2;
    }


    /**
     * @return float
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param float $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }


    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFichtech()
    {
        return $this->fichtech;
    }

    /**
     * @param string $fichtech
     */
    public function setFichtech($fichtech)
    {
        $this->fichtech = $fichtech;
    }

    /**
     * @return mixed
     */
    public function getBoutique()
    {
        return $this->boutique;
    }

    /**
     * @param mixed $boutique
     */
    public function setBoutique($boutique)
    {
        $this->boutique = $boutique;
    }

}
