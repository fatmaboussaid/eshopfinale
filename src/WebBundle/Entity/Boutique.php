<?php

namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use WebBundle\Entity\SecteurActivity;
/**
 * boutique
 *
 * @ORM\Table("boutique")
 * @ORM\Entity(repositoryClass="WebBundle\Repository\BoutiqueRepository")
 * @Vich\Uploadable
 */
class Boutique
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="raison_sociale", type="string", length=10)
     */
    private $raisonsociale;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_domaine", type="string", length=125, nullable=true)
     */
    private $nomdomaine="";

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=125)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=125)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="integer")
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="couleur", type="string", length=125, nullable=true)
     */
    private $couleur;
    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=250)
     */
    private $adresse;

    /**
     *
     *
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo = null;

    /**
     * @Vich\UploadableField(mapping="logo_images", fileNameProperty="logo")
     * @Assert\Expression("this.getLogoFile() or this.getLogo()", message="admin.validation.image.upload")
     * @Assert\File(maxSize="2000000")
     * @var File
     */
    private $logoFile;

    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\SecteurActivity", cascade={"persist"})
     * @ORM\JoinColumn(name="secteur_id", referencedColumnName="id")
     */
    private $secteur;
    /**
     * @ORM\OneToOne(targetEntity=Utilisateurs::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $responsable;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getRaisonsociale()
    {
        return $this->raisonsociale;
    }

    /**
     * @param string $raisonsociale
     */
    public function setRaisonsociale($raisonsociale)
    {
        $this->raisonsociale = $raisonsociale;
    }

    /**
     * @return string
     */
    public function getNomdomaine()
    {
        return $this->nomdomaine;
    }

    /**
     * @param string $nomdomaine
     */
    public function setNomdomaine($nomdomaine)
    {
        $this->nomdomaine = $nomdomaine;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param string $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return string
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * @param string $couleur
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    }
    /**
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }


    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return boutique
     */
    public function setLogoFile(File $logo = null)
    {
        $this->logoFile = $logo;
    }

    /**
     * @return File|null
     */
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * @return mixed
     */
    public function getSecteur()
    {
        return $this->secteur;
    }

    /**
     * @param mixed $secteur
     */
    public function setSecteur(SecteurActivity $secteur)
    {
        $this->secteur = $secteur;
    }

    /**
     * @return mixed
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * @param mixed $responsable
     */
    public function setResponsable(Utilisateurs $responsable)
    {
        $this->responsable = $responsable;
    }


}
