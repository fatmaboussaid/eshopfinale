<?php


namespace WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**

 *
 * @ORM\Table("Commande_Produit")
 * @ORM\Entity(repositoryClass="WebBundle\Repository\Commande_ProduitRepository")
 */
class Commande_Produit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\Commandes", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $commande;
    /**
     * @ORM\ManyToOne(targetEntity="WebBundle\Entity\Produits", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;
    /**
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCommande()
    {
        return $this->commande;
    }

    /**
     * @param mixed $commande
     */
    public function setCommande(Commandes $commande)
    {
        $this->commande = $commande;
    }

    /**
     * @return mixed
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * @param mixed $produit
     */
    public function setProduit(Produits $produit)
    {
        $this->produit = $produit;
    }

    /**
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;
    }

}