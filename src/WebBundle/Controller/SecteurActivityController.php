<?php

namespace WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\SecteurActivity;
use WebBundle\Form\SecteurActivityType;

/**
 * SecteurActivity controller.
 *
 * @Route("backend/SecteurActivity")
 */
class SecteurActivityController extends Controller
{
    /**
     * Lists all SecteurActivity entities.
     *
     * @Route("/", name="secteuractivity_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $secteurActivities = $em->getRepository('WebBundle:SecteurActivity')->findAll();

        return $this->render('secteuractivity/index.html.twig', array(
            'secteurActivities' => $secteurActivities,
        ));
    }

    /**
     * Creates a new SecteurActivity entity.
     *
     * @Route("/new", name="secteuractivity_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $secteurActivity = new SecteurActivity();
        $form = $this->createForm('WebBundle\Form\SecteurActivityType', $secteurActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($secteurActivity);
            $em->flush();

            return $this->redirectToRoute('secteuractivity_show', array('id' => $secteurActivity->getId()));
        }

        return $this->render('secteuractivity/new.html.twig', array(
            'secteurActivity' => $secteurActivity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SecteurActivity entity.
     *
     * @Route("/{id}", name="secteuractivity_show")
     * @Method("GET")
     */
    public function showAction(SecteurActivity $secteurActivity)
    {
        $deleteForm = $this->createDeleteForm($secteurActivity);

        return $this->render('secteuractivity/show.html.twig', array(
            'secteurActivity' => $secteurActivity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SecteurActivity entity.
     *
     * @Route("/{id}/edit", name="secteuractivity_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SecteurActivity $secteurActivity)
    {
        $deleteForm = $this->createDeleteForm($secteurActivity);
        $editForm = $this->createForm('WebBundle\Form\SecteurActivityType', $secteurActivity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($secteurActivity);
            $em->flush();

            return $this->redirectToRoute('secteuractivity_edit', array('id' => $secteurActivity->getId()));
        }

        return $this->render('secteuractivity/edit.html.twig', array(
            'secteurActivity' => $secteurActivity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a SecteurActivity entity.
     *
     * @Route("/{id}", name="secteuractivity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SecteurActivity $secteurActivity)
    {
        $form = $this->createDeleteForm($secteurActivity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($secteurActivity);
            $em->flush();
        }

        return $this->redirectToRoute('secteuractivity_index');
    }

    /**
     * Creates a form to delete a SecteurActivity entity.
     *
     * @param SecteurActivity $secteurActivity The SecteurActivity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SecteurActivity $secteurActivity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('secteuractivity_delete', array('id' => $secteurActivity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
