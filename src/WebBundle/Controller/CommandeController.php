<?php

namespace BoutiqueBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\Commande_Produit;
use WebBundle\Form\ProduitsType;

/**
 * Produits controller.
 *
 * @Route("/back_commande")
 */
class CommandeController extends Controller
{
    /**
     * Lists all Produits entities.
     *
     * @Route("/", name="commande_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="commande_index_paginated")
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('WebBundle:Commande_Produit')->GetCommandeByBoutique(1);
        $paginator = $this->get('knp_paginator');

        $commandes = $paginator->paginate(
            $query, $page, 10
        );
        $commandes->setUsedRoute('commande_index_paginated');
        return $this->render('@Boutique/commande/index.html.twig', array(
            'commandes' => $commandes,
        ));
    }

    /**
     * Creates a new Produits entity.
     *
     * @Route("/new", name="produits_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $produit = new Produits();
        $form = $this->createForm('BoutiqueBundle\Form\ProduitsType', $produit)
        ->remove('nb_achat');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $produit->getImage();
            $file1 = $produit->getImage1();
            $file2 = $produit->getImage2();
            if ($file != NULL) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                $file->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage($fileName);
            }
            if ($file1 != NULL) {
                $fileName = md5(uniqid()) . '.' . $file1->guessExtension();

                $file1->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage1($fileName);
            }
            if ($file2 != NULL) {
                $fileName = md5(uniqid()) . '.' . $file2->guessExtension();

                $file2->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage2($fileName);
            }
            $produit->setNbAchat(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('produits_show', array('id' => $produit->getId()));
        }

        return $this->render('@Boutique/produits/new.html.twig', array(
            'produit' => $produit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Produits entity.
     *
     * @Route("/show/{id}", name="commande_show", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="commandes_show_paginated")
     * @Method("GET")
     */
    public function showAction( $id,$page)
    {
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        $query= $em->getRepository('WebBundle:Commande_Produit')->GetProduitByCommande($commande);
        $paginator = $this->get('knp_paginator');
        $commande_produit = $paginator->paginate(
            $query, $page, 10
        //Produits::NUM_ITEMS
        );
        return $this->render('@Boutique/commande/show.html.twig', array(
           'commande' => $commande,
          'commande_produit' => $commande_produit,
            ));
    }

    /**
     * Displays a form to edit an existing Produits entity.
     *
     * @Route("/{id}/{statut}", name="commnde_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction($id,$statut )
    {

        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        if ($statut==2){

            $commande->setDeliveryDate(new \DateTime());
        }
        elseif ($statut==1){
            $commande->setDateValidGest(new \DateTime());
        }
        $commande->setStatusGest($statut);
        $em->merge($commande);
        $em->flush();
        return $this->redirectToRoute('commande_index');
    }

    /**
     * Deletes a Produits entity.
     *
     * @Route("/del/{id}", name="commande_delete")
     */
    public function deleteAction($id)
    {

            $em = $this->getDoctrine()->getManager();
            $commande = $em->getRepository('WebBundle:Commandes')->find($id);
            $em->remove($commande);
            $em->flush();
            $this->addFlash('success', 'Commande efface avec succes');

        return $this->redirectToRoute('commande_index');
    }

    /**
     * Creates a form to delete a Produits entity.
     *
     * @param Produits $produit The Produits entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Produits $produit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('produits_delete', array('id' => $produit->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
