<?php

namespace WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\Utilisateurs;
use WebBundle\Form\UtilisateursType;

/**
 * Utilisateurs controller.
 *
 * @Route("backend/utilisateurs")
 */
class UtilisateursController extends Controller
{
    /**
     * Lists all Utilisateurs entities.
     *
     * @Route("/", name="utilisateurs_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateurs = $em->getRepository('WebBundle:Utilisateurs')->findAll();

        return $this->render('utilisateurs/index.html.twig', array(
            'utilisateurs' => $utilisateurs,
        ));
    }

        /**
     * enable user.
     *
     * @Route("/{id}/enable", name="utilisateurs_enable")
     * @Method("GET")
     */
    public function enableAction(Utilisateurs $utilisateur)
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateurs = $em->getRepository('WebBundle:Utilisateurs')->findAll();
        $oneUser=$em->getRepository('WebBundle:Utilisateurs')->findOneBy(array('id'=>$utilisateur->getId()));
        $oneUser->setEnabled(1);
        $em->persist($oneUser);
        $em->flush();
        return $this->render('utilisateurs/index.html.twig', array(
            'utilisateurs' => $utilisateurs,
        ));
    }

         /**
     * enable user.
     *
     * @Route("/{id}/disable", name="utilisateurs_disable")
     * @Method("GET")
     */
    public function disableAction(Utilisateurs $utilisateur)
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateurs = $em->getRepository('WebBundle:Utilisateurs')->findAll();
        $oneUser=$em->getRepository('WebBundle:Utilisateurs')->findOneBy(array('id'=>$utilisateur->getId()));
        $oneUser->setEnabled(0);
        $em->persist($oneUser);
        $em->flush();
        return $this->render('utilisateurs/index.html.twig', array(
            'utilisateurs' => $utilisateurs,
        ));
    }

    /**
     * Creates a new Utilisateurs entity.
     *
     * @Route("/new", name="utilisateurs_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $utilisateur = new Utilisateurs();
        $form = $this->createForm('WebBundle\Form\UtilisateursType', $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($utilisateur);
            $em->flush();

            return $this->redirectToRoute('utilisateurs_show', array('id' => $utilisateur->getId()));
        }

        return $this->render('utilisateurs/new.html.twig', array(
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Utilisateurs entity.
     *
     * @Route("/{id}", name="utilisateurs_show")
     * @Method("GET")
     */
    public function showAction(Utilisateurs $utilisateur)
    {
        $deleteForm = $this->createDeleteForm($utilisateur);

        return $this->render('utilisateurs/show.html.twig', array(
            'utilisateur' => $utilisateur,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Utilisateurs entity.
     *
     * @Route("/{id}/edit", name="utilisateurs_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Utilisateurs $utilisateur)
    {
        $deleteForm = $this->createDeleteForm($utilisateur);
        $editForm = $this->createForm('WebBundle\Form\UtilisateursType', $utilisateur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($utilisateur);
            $em->flush();

            return $this->redirectToRoute('utilisateurs_edit', array('id' => $utilisateur->getId()));
        }

        return $this->render('utilisateurs/edit.html.twig', array(
            'utilisateur' => $utilisateur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Utilisateurs entity.
     *
     * @Route("/{id}", name="utilisateurs_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Utilisateurs $utilisateur)
    {
        $form = $this->createDeleteForm($utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($utilisateur);
            $em->flush();
        }

        return $this->redirectToRoute('utilisateurs_index');
    }

    /**
     * Creates a form to delete a Utilisateurs entity.
     *
     * @param Utilisateurs $utilisateur The Utilisateurs entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Utilisateurs $utilisateur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('utilisateurs_delete', array('id' => $utilisateur->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
