<?php

namespace BoutiqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/login_back")
 */
class SecurityController extends Controller
{
    /**
     * @Route(path="/", name="login_back")
     */
    public function indexAction()
    {
        return $this->render('BoutiqueBundle::login.html.twig');
    }
}
