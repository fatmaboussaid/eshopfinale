<?php

namespace BoutiqueBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\Produits;
use WebBundle\Form\ProduitsType;

/**
 * Produits controller.
 *
 * @Route("/back_produits")
 */
class ProduitsController extends Controller
{
    /**
     * Lists all Produits entities.
     *
     * @Route("/", name="produits_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="produits_index_paginated")
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('WebBundle:Produits')->findAll();
        $paginator = $this->get('knp_paginator');
        $produits = $paginator->paginate(
            $query, $page, 9
        //Produits::NUM_ITEMS
        );
        $produits->setUsedRoute('produits_index_paginated');
        return $this->render('@Boutique/produits/index.html.twig', array(
            'produits' => $produits,
        ));
    }

    /**
     * Creates a new Produits entity.
     *
     * @Route("/new", name="produits_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $produit = new Produits();
        $form = $this->createForm('BoutiqueBundle\Form\ProduitsType', $produit)
        ->remove('nb_achat');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $produit->getImage();
            $file1 = $produit->getImage1();
            $file2 = $produit->getImage2();
            if ($file != NULL) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                $file->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage($fileName);
            }
            if ($file1 != NULL) {
                $fileName = md5(uniqid()) . '.' . $file1->guessExtension();

                $file1->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage1($fileName);
            }
            if ($file2 != NULL) {
                $fileName = md5(uniqid()) . '.' . $file2->guessExtension();

                $file2->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage2($fileName);
            }
            $produit->setNbAchat(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('produits_show', array('id' => $produit->getId()));
        }

        return $this->render('@Boutique/produits/new.html.twig', array(
            'produit' => $produit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Produits entity.
     *
     * @Route("/show", name="produits_show")
     * @Method("GET")
     */
    public function showAction(Request $request )
    {
       // dump( $request->query->get('id'));die();
        $id = $request->query->get('id');
        $em = $this->getDoctrine()->getManager();
        $produit = $em->getRepository('WebBundle:Produits')->find($id);
        $deleteForm = $this->createDeleteForm($produit);

        return $this->render('@Boutique/produits/show.html.twig', array(
            'produit' => $produit,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Produits entity.
     *
     * @Route("/{id}/edit", name="produits_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Produits $produit)
    {
        $editForm = $this->createForm('BoutiqueBundle\Form\ProduitsType', $produit)
        ->remove('nb_achat');

        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $file = $produit->getImage();
            $file1 = $produit->getImage1();
            $file2 = $produit->getImage2();
            if ($file != NULL) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                $file->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage($fileName);
            }
            if ($file1 != NULL) {
                $fileName = md5(uniqid()) . '.' . $file1->guessExtension();

                $file1->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage1($fileName);
            }
            if ($file2 != NULL) {
                $fileName = md5(uniqid()) . '.' . $file2->guessExtension();

                $file2->move(
                    $this->getParameter('image_directory'), $fileName
                );

                $produit->setImage2($fileName);
            }
            $em = $this->getDoctrine()->getManager();
         //   $p = $em->getRepository('WebBundle:Produits')->find($produit->getId());
          //  $produit->setNbAchat($p->getNbAchat());
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('produits_index');
        }

        return $this->render('@Boutique/produits/edit.html.twig', array(
            'produit' => $produit,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Produits entity.
     *
     * @Route("/del/{id}", name="produits_delete")
     */
    public function deleteAction(Request $request, Produits $produit)
    {

            $em = $this->getDoctrine()->getManager();
            $em->remove($produit);
            $em->flush();
            $this->addFlash('success', 'Produit efface avec succes');
            $produits = $em->getRepository('WebBundle:Produits')->findAll();

        return $this->redirectToRoute('produits_index');
    }

    /**
     * Creates a form to delete a Produits entity.
     *
     * @param Produits $produit The Produits entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Produits $produit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('produits_delete', array('id' => $produit->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
