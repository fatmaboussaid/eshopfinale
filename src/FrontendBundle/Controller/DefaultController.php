<?php

namespace FrontendBundle\Controller;

use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use WebBundle\Entity\ContactUsFormClass;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController
{
    /**
     * @Route("/", name="homepage_frontend")
     */
    public function indexAction()
    {
        // get top produits
       /*  $produits = $this->getDoctrine()->getRepository('WebBundle:Produits')->getTopProduits(5);
        $request = $this->container->get('request');
        $routeName = $request->get('_route'); */
        
        return $this->render('FrontendBundle:Default:index.html.twig');
    }
    /**
     * @Route("/demande", name="demande_frontend")
     */
    public function demandeAction()
    {
        // get top produits
       /*  $produits = $this->getDoctrine()->getRepository('WebBundle:Produits')->getTopProduits(5);
        $request = $this->container->get('request');
        $routeName = $request->get('_route'); */
        
        return $this->render('FrontendBundle:addBoutique:addboutique.html.twig');
    }
    /**
     * @Route("/contact", name="contact_us")
     */
    public function contactAction(Request $request)
    {
        // create a task and give it some dummy data for this example
        $contactEntity = new ContactUsFormClass();


        $form = $this->createFormBuilder($contactEntity)
            ->add('civilite', ChoiceType::class,
                array('choices' => array(
                    'M.' => 'Monsieur',
                    'Mme.' => 'Madame'),
                    'choices_as_values' => true,
                    'multiple'=>false,
                    'expanded'=>true))
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('adresse', TextType::class)
            ->add('ville', TextType::class)
            ->add('zone', TextType::class)
            ->add('email', EmailType::class,array('required' => true ))
            ->add('tel', TextType::class,array('required' => true ))
            ->add('object', ChoiceType::class,array(
                'required' => true,
                'multiple'=>true,
                'choices'=>array(
                    'Prix'=>'Prix',
                    'Réclamation'=>'Réclamation',
                    'Assistance technique'=>'Assistance technique',
                    'Formation'=>'Formation',
                    'Autre'=>'Autre',
                ),
            ))
            ->add('sujet', TextType::class,array('required' => true ))
            ->add('message', TextareaType::class)
            ->add('captcha', CaptchaType::class,array(
                'reload' => true,
                'as_url' => true,
            ))
            ->getForm();
        $done = 0;
        if ($this->get('request')->getMethod() == 'POST') {
            $form->handleRequest($this->getRequest());
            //var_dump($form->getData());die();
            if ($form->isValid()) {
                $sexe = $contactEntity->getCivilite();
                $nom = $contactEntity->getNom();
                $prenom = $contactEntity->getPrenom();
                $ville = $contactEntity->getVille();
                $zone = $contactEntity->getZone();
                $email = $contactEntity->getEmail();
                $tel = $contactEntity->getTel();
                $objet = $contactEntity->getObject();
                $adresse = $contactEntity->getAdresse();
                $sujet = $contactEntity->getSujet();
                $message = $contactEntity->getMessage();


                $message = \Swift_Message::newInstance()
                    ->setSubject('Contact utilisateur')
                    ->setFrom(array($email => "Utilisateur Total B2B"))
                    ->setTo("b2b.total@gmail.com")
                    ->setCharset('utf-8')
                    ->setContentType('text/html')
                    ->setBody($this->renderView('FrontendBundle:SwiftLayout:contactContent.html.twig', array(
                        'sexe' => $sexe,
                        'nom' => $nom,
                        'prenom' => $prenom,
                        'adresse' => $adresse,
                        'ville' => $ville,
                        'zone' => $zone,
                        'email' => $email,
                        'tel' => $tel,
                        'objet' => $objet,
                        'sujet' => $sujet,
                        'msg' => $message)), 'text/html');

                $this->get('mailer')->send($message);
                $done = 1;
            }
        }
        $request = $this->container->get('request');
        $routeName = $request->get('_route');
        return $this->render('FrontendBundle:Default:contact.html.twig',array('cp'=>$routeName,'form' => $form->createView(),'done'=>$done));
    }


    /**
     * @Route("/cart")
     */
    public function cartAction()
    {
        return $this->render('@Frontend/Default/cart.html.twig');
    }

    /**
     * @Route("/produits", name="list_produits")
     */
    public function produitsAction()
    {
        return $this->render('FrontendBundle:Default:produits.html.twig');
    }

    /**
     * @Route("/detailsproduit", name="details_produit")
     */
    public function detailsproduitAction()
    {
        return $this->render('FrontendBundle:Default:detailsproduit.html.twig');
    }


   

}
