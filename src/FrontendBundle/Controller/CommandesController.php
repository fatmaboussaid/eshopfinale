<?php

namespace FrontendBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WebBundle\Entity\Commandes;
use WebBundle\Entity\Commandes_grossiste;

class CommandesController extends BaseController
{
    public function facture()
    {
        $em = $this->getDoctrine()->getManager();
        $generator = $this->container->get('security.secure_random');
        $session = $this->getRequest()->getSession();
        $adresse = $session->get('adresse');
        $panier = $session->get('panier');


        $commande = array();
        $totalHT = 0;

        $facturation = $em->getRepository('WebBundle:UtilisateursAdresses')->find($adresse['facturation']);
        $livraison = $em->getRepository('WebBundle:UtilisateursAdresses')->find($adresse['livraison']);
        $produits = $em->getRepository('WebBundle:Produits')->findArray(array_keys($session->get('panier')));

        foreach($produits as $produit)
        {
            $prixHT = ($produit->getPrix() * $panier[$produit->getId()]);
            $totalHT += $prixHT;

            $commande['produit'][$produit->getId()] = array(
                'id'=>$produit->getId(),
                'categorie'=> $produit->getCategorie()->getNom(),
                'reference' => $produit->getNom(),
                'unite' => $produit->getUnite()->getLibelle(),
                'quantite' => $panier[$produit->getId()],
                'prixHT' => round($produit->getPrix(),2),
                'image' => $produit->getPathImage());
        }

        $commande['livraison'] = array('prenom' => $livraison->getPrenom(),
            'nom' => $livraison->getNom(),
            'telephone' => $livraison->getTelephone(),
            'adresse' => $livraison->getAdresse(),
            'cp' => $livraison->getCp(),
            'ville' => $livraison->getVille(),
            'pays' => $livraison->getPays(),
            'complement' => $livraison->getComplement());

        $commande['facturation'] = array('prenom' => $facturation->getPrenom(),
            'nom' => $facturation->getNom(),
            'telephone' => $facturation->getTelephone(),
            'adresse' => $facturation->getAdresse(),
            'cp' => $facturation->getCp(),
            'ville' => $facturation->getVille(),
            'pays' => $facturation->getPays(),
            'complement' => $facturation->getComplement());


        $commande['prixHT'] = round($totalHT,2);
        $commande['token'] = bin2hex($generator->nextBytes(20));

        return $commande;
    }



    public function prepareCommandeAction()
    {

        $session = $this->initSession();
        $em = $this->initEntityManager();
        $date = $this->initDate();
       // var_dump($session->has('commande'));
        //var_dump($session->get('commande'));
        //exit();
        if (!$session->has('commande')) {
            //var_dump($session->has('commande'));exit();
            $commande = new Commandes();
            //var_dump($commande);exit();
    }
        else {
            //var_dump($session->get('commande'));exit();
            $commande = $em->getRepository('WebBundle:Commandes')->find($session->get('commande'));
            //var_dump($commande);exit();
        }
        //$commande = new Commandes();
       // var_dump($commande);die();
        $commande->setUtilisateur($this->getUser());
        $commande->setValider(0);
        $commande->setReference(0);
        $commande->setCommande($this->facture());
        $commande->setDateReservation(\DateTime::createFromFormat('Y-m-d', $date["date"]));
        $commande->setPeriodeReservation($date['periode']);
        //var_dump($commande);exit();
        if (!$session->has('commande')) {
            $em->persist($commande);
            $session->set('commande',$commande);
        }
        
        $em->flush();
        //var_dump(Response($commande->getId())); die();
        return new Response($commande->getId());
    }
    
    /*
     * Cette methode remplace l'api banque.
     */
    /**
     * @Route(path="/mail_test/{id}",  name="send_mail_test")
     */
    public function sendMailTestAction($id)
    {
        $em = $this->initEntityManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        //Ici le mail de validation
        $message = \Swift_Message::newInstance()
            ->setSubject('Validation de votre commande')
            ->setFrom(array('b2b.total@gmail.com' => "Total B2B"))
            ->setTo("yosri.mekni.sidratsoft@gmail.com")
            ->setCharset('utf-8')
            ->setContentType('text/html')
            ->setBody($this->renderView('FrontendBundle:SwiftLayout:validationCommande.html.twig',array('commande' => $commande,)), 'text/html');

        $this->get('mailer')->send($message);
       // $result = $mailer->send($message);
        //var_dump($result);die();
        return $this->render('FrontendBundle:SwiftLayout:validationCommande.html.twig'
            ,array('commande' => $commande));
    }
    /**
     * @Route(path="/validation_commande/{id}",  name="validation_commande")
     */
    public function validationCommandeAction($id)
    {
        $em = $this->initEntityManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        
        if (!$commande || $commande->getValider() == 1)
            throw $this->createNotFoundException('La commande n\'existe pas');
        
        $commande->setValider(1);
        $commande->setReference($this->container->get('setNewReference')->reference()); //Service
        $delegation_grossiste = $em->getRepository('WebBundle:Delegation')->findOneBy(array('code'=>$commande->getCommande()["livraison"]["cp"]))->getGrossiste();
        $commande->setDeliveryDate($commande->getDateReservation());

        if ($delegation_grossiste!= null)$commande->setGrossiste($delegation_grossiste); else $commande->setGrossiste(null);
        $em->flush();   
        
        $session = $this->initSession();
        $panier = $this->initPanier();
        $adresse = $this->initAdresse();
        $s_commande = $this->initCommande();

        $produits = $this->getProduitsByIds(array_keys($panier));

        $session->remove('adresse');
        $session->remove('panier');
        $session->remove('commande');
        $session->remove('date');

        //Ici le mail de validation
        $message = \Swift_Message::newInstance()
                ->setSubject('Commande en cours de traitement')
                ->setFrom(array('b2b.total@gmail.com' => "Total B2B"))
                ->setTo($commande->getUtilisateur()->getEmailCanonical())
                ->setCharset('utf-8')
                ->setContentType('text/html')
                ->setBody($this->renderView('FrontendBundle:SwiftLayout:validationCommande.html.twig',array('commande' => $commande)));

        $this->get('mailer')->send($message);
        
        $this->get('session')->getFlashBag()->add('success','Votre commande est validé avec succès');


        return $this->render('FrontendBundle:Panier:thanks.html.twig',
            array(
                'produits' => $produits,
                'totale' => $commande->getCommande()['prixHT'],
                'panier' => $panier,
                "reference" => $commande->getReference()
            ));
    }
}
