<?php

namespace FrontendBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/produit")
 */
class ProduitController extends BaseController
{
    /**
     * @Route(path="/", name="list_produit")
     */
    public function indexAction(Request $request)
    {
        $this->initEntityManager();
        $this->initSession();


        $categorie_id = $this->getRequest()->query->get('categorie_id',"");
      //  $unite_id = $this->getRequest()->query->get('unite_id',"");
        $keyword = $this->getRequest()->query->get('keyword',"");
        $max_price = $this->getRequest()->query->get('max_price',"");
        $min_price = $this->getRequest()->query->get('min_price',"");
        $sort = $this->getRequest()->query->get('sort',"");

       switch ($sort){
           case 1: $sort_by="created_at"; break;
           case 2: $sort_by="nb_achat"; break;
           case 3: $sort_by="prix"; break;
           default: $sort_by="";
       }



        $categories = $this->getAllCategories();
      //  $unites = $this->getAllUnites();
     //   $findProduits = $this->getProduitsFiltred($categorie_id, $min_price, $max_price, $sort_by,$keyword);
        $findProduits = "";
        if ($this->session->has('panier'))
            $panier = $this->session->get('panier');
        else
            $panier = false;

        $produits = $this->get('knp_paginator')->paginate($findProduits, $this->get('request')->query->get('page', 1), 9);

        $request = $this->container->get('request');
        $routeName = $request->get('_route');
        return $this->render('FrontendBundle:Produit:index.html.twig', array('produits' => $produits,
        'panier' => $panier, "categories" => $categories,'cp'=>$routeName));
    }


    /**
     * @Route(path="/{id}", name="show_produit")
     */
    public function showAction(Request $request, $id)
    {
        $em = $this->initEntityManager();
        $produit = $this->getOneProduitById($id);
        $produits = $this->getProduitBy(array("categorie_id" => $produit->getCategorieId()));
        $produits_similaire = $this->get('knp_paginator')->paginate($produits, $this->get('request')->query->get('page', 1), 4);

        return $this->render('FrontendBundle:Produit:show.html.twig',array("produit" => $produit, "produits_similaire" => $produits_similaire,'cp'=>"produitsPage"));
    }

}
