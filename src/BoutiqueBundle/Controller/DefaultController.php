<?php

namespace BoutiqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Boutique;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class DefaultController extends Controller
{

    /**
     * @Route("/index", name="boutique_backend")
     */
    public function indexAction()
    {
        return $this->render('BoutiqueBundle::createboutique.html.twig');
    }
    public function create_boutiqueAction()
    {
        //$secteurs= $this->getDoctrine()->getRepository('WebBundle:SecteurActivity')->findAll();

        return $this->render('BoutiqueBundle::boutique.html.twig');
    }
    /**
     * Creates a new boutique entity.
     *
     * @Route("/", name="new_boutique")
     * @Method({"GET", "POST"})
     *
     */
    public function newboutiqueAction(Request $request){
        $boutique =new Boutique();
      //  dump('aaaa');die();
        $form = $this->createForm('BoutiqueBundle\Form\BoutiqueType', $boutique)
            ->add('Valider', 'Symfony\Component\Form\Extension\Core\Type\SubmitType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $file = $boutique->getLogo();
            if ($file != NULL) {

                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                $file->move(
                    $this->getParameter('logo_directory'), $fileName
                );

                $boutique->setLogo($fileName);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($boutique);
            $entityManager->flush();

            // See http://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'Le boutique crée avec succes');

            if ($form->get('Valider')->isClicked()) {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Validation du compte')
                    ->setFrom(array('fatma.boussaid@gmail.com' => "Total B2B"))
                    ->setTo($boutique->getEmail())
                    ->setCharset('utf-8')
                    ->setContentType('text/html')
                    ->setBody($this->renderView('BoutiqueBundle:email:validationemail.html.twig', array('boutique' => $boutique)));

                $this->get('mailer')->send($message);
                return $this->redirectToRoute('boutique_backend');
            }

            return $this->redirectToRoute('boutique_backend');
        }

        return $this->render('@Boutique/createboutique.html.twig', array(
            'post' => $boutique,
            'form' => $form->createView(),
        ));
    }
    /**
     * @Route("register/{boutique}", name="registerresp")
     */
    public function RegistrationAction($boutique)
    {
        return $this->render('BoutiqueBundle::registre.html.twig');
    }
}
