<?php

namespace BoutiqueBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\Commande_Produit;
use WebBundle\Form\ProduitsType;

/**
 * Produits controller.
 *
 * @Route("/back_historique")
 */
class HistoriqueController extends Controller
{
    /**
     * Lists all Produits entities.
     *
     * @Route("/", name="historique_index", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="commande_index_paginated")
     * @Method("GET")
     */
    public function indexAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('WebBundle:Commande_Produit')->GetCommandeHistorique(1);
        $paginator = $this->get('knp_paginator');
        $commandes = $paginator->paginate(
            $query, $page, 2
        );
        $commandes->setUsedRoute('commande_index_paginated');
        return $this->render('@Boutique/historique/index.html.twig', array(
            'commandes' => $commandes,
        ));
    }

    /**
     * Finds and displays a Produits entity.
     *
     * @Route("/show/{id}", name="historique_show", defaults={"page": 1})
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="commandes_show_paginated")
     * @Method("GET")
     */
    public function showAction( $id,$page)
    {
        $em = $this->getDoctrine()->getManager();
        $commande = $em->getRepository('WebBundle:Commandes')->find($id);
        $query= $em->getRepository('WebBundle:Commande_Produit')->GetProduitByCommande($commande);
        $paginator = $this->get('knp_paginator');
        $commande_produit = $paginator->paginate(
            $query, $page, 2
        //Produits::NUM_ITEMS
        );
        return $this->render('@Boutique/historique/show.html.twig', array(
           'commande' => $commande,
          'commande_produit' => $commande_produit,
            ));
    }

    /**
     * Deletes a Produits entity.
     *
     * @Route("/del/{id}", name="historique_delete")
     */
    public function deleteAction($id)
    {

            $em = $this->getDoctrine()->getManager();
            $commande = $em->getRepository('WebBundle:Commandes')->find($id);
            $em->remove($commande);
            $em->flush();
            $this->addFlash('success', 'Commande efface avec succes');

        return $this->redirectToRoute('commande_index');
    }

}
