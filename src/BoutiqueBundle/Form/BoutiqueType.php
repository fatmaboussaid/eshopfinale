<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BoutiqueBundle\Form;

use Imagine\Image\Palette\Color\CMYK;
use Imagine\Image\Palette\Color\ColorInterface;
use Sonata\CoreBundle\Form\Type\ColorSelectorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\Email;
use Vich\UploaderBundle\Form\Type\VichFileType;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class BoutiqueType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // see http://symfony.com/doc/current/reference/forms/types.html
        // $builder->add('title', null, array('required' => false, ...));

        $builder
            ->add('nom', null, array(
                    'attr' => array('autofocus' => true),
                    'label' => 'Nom',
                ))
            ->add('raisonsociale', null, array(
                'attr' => array('autofocus' => true),
                'label' => 'Raison Sociale',
            ))
            ->add('tel', null, array(
                'attr' => array('autofocus' => true),
                'label' => 'Telephone ',
            ))

            ->add('nomdomaine', null, array(
                'attr' => array('autofocus' => true),
                'label' => 'Nom Domaine ',
            ))

            ->add('email', 'Symfony\Component\Form\Extension\Core\Type\EmailType', array(
                'attr' => array('autofocus' => true),
                'label' => 'Email ',
            ))

                ->add('description', 'Symfony\Component\Form\Extension\Core\Type\TextareaType',
                    array('label' => 'Description'))


                ->add('secteur', EntityType::class, array(
                    'class' => 'WebBundle\Entity\SecteurActivity',
                    'choice_label' => 'getNom',
                ))
                ->add('logo', FileType::class, array('label' => 'Logo', 'required' => FALSE, 'data_class' => NULL))


        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebBundle\Entity\Boutique',
        ));
    }

}
